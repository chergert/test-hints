#include <gtk/gtk.h>

#include "test-widget.h"

int
main (int argc,
      char *argv[])
{
  GMainLoop *main_loop;
  GtkWindow *window;
  GtkScrolledWindow *scroller;
  GtkBox *box;
  TestWidget *child;
  char *contents;
  const char *end;
  GError *error = NULL;
  gsize len;
  char **lines;

  if (argc < 2)
    {
      g_printerr ("usage: %s filename\n", argv[0]);
      return 1;
    }

  if (!g_file_get_contents (argv[1], &contents, &len, &error))
    {
      g_printerr ("Failed to load file: %s: %s\n", argv[1], error->message);
      return 1;
    }

  if (!g_utf8_validate (contents, len, &end))
    {
      g_printerr ("%s contains invalid UTF-8 at position %"G_GINT64_FORMAT"\n",
                  argv[1], (gint64)(end - contents));
      return 1;
    }

  gtk_init ();

  main_loop = g_main_loop_new (NULL, FALSE);
  window = g_object_new (GTK_TYPE_WINDOW,
                         "title", "Scroll Testing",
                         "default-width", 800,
                         "default-height", 1000,
                         NULL);
  scroller = g_object_new (GTK_TYPE_SCROLLED_WINDOW,
                           NULL);
  gtk_window_set_child (window, GTK_WIDGET (scroller));

  box = g_object_new (GTK_TYPE_BOX,
                      "orientation", GTK_ORIENTATION_VERTICAL,
                      NULL);
  gtk_scrolled_window_set_child (scroller, GTK_WIDGET (box));

  lines = g_strsplit (contents, "\n", 0);
  child = test_widget_new ();
  gtk_box_append (box, GTK_WIDGET (child));
  for (gsize i = 0; lines[i]; i++)
    {
      if (i % 50 == 0)
        {
          if (lines[i+1] != NULL)
            {
              child = test_widget_new ();
              gtk_box_append (box, GTK_WIDGET (child));
            }
        }

      test_widget_append_line (child, i+1, lines[i]);
    }
  g_strfreev (lines);

  gtk_window_present (window);
  g_signal_connect_swapped (window, "close-request", G_CALLBACK (g_main_loop_quit), main_loop);
  g_main_loop_run (main_loop);

  return 0;
}
