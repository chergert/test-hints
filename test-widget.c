#include "test-widget.h"

struct _TestWidget
{
  GtkWidget parent_instance;
  GArray *lines;
  GskRenderNode *node;
  guint needs_layout : 1;
  PangoFontDescription *font;
};

typedef struct
{
  char *line;
  PangoLayout *layout;
  int height;
} Line;

G_DEFINE_TYPE (TestWidget, test_widget, GTK_TYPE_WIDGET)

static int before_line = 2;
static int after_line = 2;

static void
test_widget_snapshot (GtkWidget   *widget,
                      GtkSnapshot *snapshot)
{
  TestWidget *self = (TestWidget *)widget;

  if (self->node == NULL)
    {
      GtkSnapshot *sub;
      GskRenderNode *child;
      GdkRGBA color;

      sub = gtk_snapshot_new ();

      gtk_style_context_get_color (gtk_widget_get_style_context (widget), &color);

      for (guint i = 0; i < self->lines->len; i++)
        {
          const Line *line = &g_array_index (self->lines, Line, i);

          gtk_snapshot_translate (sub, &GRAPHENE_POINT_INIT (0, before_line));
          gtk_snapshot_append_layout (sub, line->layout, &color);
          gtk_snapshot_translate (sub, &GRAPHENE_POINT_INIT (0, line->height + after_line));
        }

      if ((child = gtk_snapshot_free_to_node (sub)))
#ifdef FORCE_CACHE
        self->node = gsk_hint_node_new (child, GSK_RENDER_HINTS_FORCE_CACHE);
#else
        self->node = child;
#endif
    }

  if (self->node)
    gtk_snapshot_append_node (snapshot, self->node);
}

static void
line_clear (gpointer data)
{
  Line *line = data;

  g_clear_pointer (&line->line, g_free);
  g_clear_object (&line->layout);
}

static void
test_widget_css_changed (GtkWidget         *widget,
                         GtkCssStyleChange *change)
{
  TestWidget *self = (TestWidget *)widget;

  for (guint i = 0; i < self->lines->len; i++)
    g_clear_object (&g_array_index (self->lines, Line, i).layout);

  self->needs_layout = TRUE;
  g_clear_pointer (&self->node, gsk_render_node_unref);
  gtk_widget_queue_resize (widget);
}

static inline void
ensure_line (TestWidget *self,
             Line       *line)
{
  if (line->layout == NULL)
    {
      line->layout = gtk_widget_create_pango_layout (GTK_WIDGET (self), line->line);
      pango_layout_set_font_description (line->layout, self->font);
    }
}

static void
test_widget_measure (GtkWidget      *widget,
                     GtkOrientation  orientation,
                     int             for_size,
                     int            *minimum,
                     int            *natural,
                     int            *minimum_baseline,
                     int            *natural_baseline)
{
  TestWidget *self = (TestWidget *)widget;

  if (self->needs_layout)
    {
      for (guint i = 0; i < self->lines->len; i++)
        {
          Line *line = &g_array_index (self->lines, Line, i);
          ensure_line (self, line);
        }
      self->needs_layout = FALSE;
    }

  if (orientation == GTK_ORIENTATION_HORIZONTAL)
    {
      int width = 0;

      for (guint i = 0; i < self->lines->len; i++)
        {
          const Line *line = &g_array_index (self->lines, Line, i);
          int w, h;

          pango_layout_set_height (line->layout, for_size);
          pango_layout_get_pixel_size (line->layout, &w, &h);
          pango_layout_set_height (line->layout, -1);

          width = MAX (width, w);
        }

      *minimum = *natural = width;
      *minimum_baseline = *natural_baseline = -1;
    }
  else
    {
      int height = 0;

      for (guint i = 0; i < self->lines->len; i++)
        {
          Line *line = &g_array_index (self->lines, Line, i);
          int w, h;

          pango_layout_set_width (line->layout, for_size);
          pango_layout_get_pixel_size (line->layout, &w, &h);
          pango_layout_set_width (line->layout, -1);

          line->height = h;

          height += before_line + h + after_line;
        }

      *minimum = *natural = height;
      *minimum_baseline = *natural_baseline = -1;
    }
}

static void
test_widget_size_allocate (GtkWidget *widget,
                           int        width,
                           int        height,
                           int        baseline)
{
  TestWidget *self = (TestWidget *)widget;

  g_clear_pointer (&self->node, gsk_render_node_unref);
  gtk_widget_queue_draw (widget);
}

static void
test_widget_finalize (GObject *object)
{
  TestWidget *self = (TestWidget *)object;

  g_clear_pointer (&self->lines, g_array_unref);
  g_clear_pointer (&self->font, pango_font_description_free);
  g_clear_pointer (&self->node, gsk_render_node_unref);

  G_OBJECT_CLASS (test_widget_parent_class)->finalize (object);
}

static void
test_widget_class_init (TestWidgetClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->finalize = test_widget_finalize;

  widget_class->snapshot = test_widget_snapshot;
  widget_class->css_changed = test_widget_css_changed;
  widget_class->measure = test_widget_measure;
  widget_class->size_allocate = test_widget_size_allocate;
}

static void
test_widget_init (TestWidget *self)
{
  self->lines = g_array_new (FALSE, FALSE, sizeof (Line));
  g_array_set_clear_func (self->lines, line_clear);

  self->font = pango_font_description_from_string ("Monospace 11");
}

TestWidget *
test_widget_new (void)
{
  return g_object_new (TEST_TYPE_WIDGET, NULL);
}

void
test_widget_append_line (TestWidget *self,
                         int         number,
                         const char *line)
{
  Line l;

  l.line = g_strdup_printf ("%6d  %s", number, line);
  l.layout = NULL;
  g_array_append_val (self->lines, l);

  self->needs_layout = TRUE;

  gtk_widget_queue_resize (GTK_WIDGET (self));
}
