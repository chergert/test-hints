all: test-scroll test-scroll-cached

test-scroll: test-widget.c test-widget.h test-scroll.c
	$(CC) -UFORCE_CACHE $(shell pkg-config --cflags --libs gtk4) test-scroll.c test-widget.c -o $@ -Wall -ggdb

test-scroll-cached: test-widget.c test-widget.h test-scroll.c
	$(CC) -DFORCE_CACHE $(shell pkg-config --cflags --libs gtk4) test-scroll.c test-widget.c -o $@ -Wall -ggdb

clean:
	rm -f test-scroll
