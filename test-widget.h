#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define TEST_TYPE_WIDGET (test_widget_get_type())

G_DECLARE_FINAL_TYPE (TestWidget, test_widget, TEST, WIDGET, GtkWidget)

TestWidget *test_widget_new         (void);
void        test_widget_append_line (TestWidget *widget,
                                     int         number,
                                     const char *line);

G_END_DECLS
